/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// React core.
import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import firebase from "firebase/app";
import "firebase/auth";

// Styles
import styles from "./app.css"; // This uses CSS modules.
import "./firebaseui-styling.global.css"; // Import globally.
import { ChannelList } from "./ChannelList";

// Get the Firebase config from the auto generated file.
const firebaseConfig = require("./firebase-config.json").result;

// Instantiate a Firebase app.
const firebaseApp = firebase.initializeApp(firebaseConfig);

const googleAuth = new firebase.auth.GoogleAuthProvider();
googleAuth.addScope("https://www.googleapis.com/auth/youtube.readonly");

const authenticate = setToken => {
  firebase
    .auth()
    .signInWithPopup(googleAuth)
    .then(function(result) {
      // The signed-in user info.
      firebase.auth().crede;
      setToken(result.credential);
      console.log(result.credential);
      // ...
    })
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...

      console.log({ errorCode, errorMessage, email, credential });
    });
};

/**
 * The Splash Page containing the login UI.
 */
const App = () => {
  const [isSignedIn, setIsSignedIn] = React.useState(undefined);
  const [channels, setChannels] = React.useState(undefined);
  const [googleToken, setGoogleToken] = React.useState(undefined);

  /**
   * @inheritDoc
   */
  React.useEffect(() => {
    const unregisterAuthObserver = firebaseApp
      .auth()
      .onAuthStateChanged(user => {
        setIsSignedIn(user || undefined);
      });

    return () => unregisterAuthObserver();
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <i className={styles.logoIcon + " material-icons"}>photo</i> Tus 10
        Segundos
      </div>
      <div className={styles.caption}>Esto es un prototipo</div>
      {(!isSignedIn || !googleToken) && (
        <div>
          <button
            onClick={() => {
              authenticate(setGoogleToken);
            }}
          >
            Sign-in
          </button>
        </div>
      )}
      {isSignedIn && googleToken && (
        <div className={styles.signedIn}>
          Hello {firebaseApp.auth().currentUser.displayName}. You are now signed
          In!
          <a
            className={styles.button}
            onClick={() => firebaseApp.auth().signOut()}
          >
            Sign-out
          </a>
          <button
            onClick={() => {
              axios
                .get(
                  "https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails%2Cstatistics&mine=true",
                  {
                    headers: {
                      Authorization: "Bearer " + googleToken.accessToken,
                      Accept: "application/json"
                    }
                  }
                )
                .then(({ error, data }) => {
                  if (error) {
                    throw error;
                  }

                  if (data) {
                    const { items: canales } = data;
                    const channels = canales.map(
                      ({ id, snippet: { title } }) => {
                        return title;
                      }
                    );

                    setChannels({
                      user: firebase.auth().currentUser.displayName,
                      channels
                    });
                  }
                })
                .catch(error => {
                  console.error(error);
                });
            }}
          >
            Get Channels
          </button>
          {!!channels ? (
            <ChannelList channels={channels.channels} user={channels.user} />
          ) : (
            <div>No channels available</div>
          )}
        </div>
      )}
    </div>
  );
};

// Load the app in the browser.
ReactDOM.render(<App />, document.getElementById("app"));

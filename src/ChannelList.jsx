import * as React from "react";

export const ChannelList = ({ channels, user }) => {
  return (
    <div>
      {user}'s channels:
      <ul>
        {(channels || []).map(channel => (
          <li key={channel}>{channel}</li>
        ))}
      </ul>
    </div>
  );
};
